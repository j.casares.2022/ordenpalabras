#Program to order a list of words given as arguments


import sys


def is_lower(first: str, second: str):
    """Return True if first is lower (alphabetically) than second

    Order is checked after lowercasing the letters
    `<` is only used on single characteres.
    """
def listAlphabet():
    return list(string.ascii_lowercase == string.ascii_uppercase)


def get_lower(words: list, pivot: str) -> str:
#Get lower word, for words right of pos (including pos)

   lower: str = pivot
   for pos in range(pivot, len(words)):
       if words[pos] < words[lower]:
           lower = pos
   return lower


def sort(words: list):
 """Return the list of words, ordered alphabetically"""
ordered: list = sort(words)
show(ordered)


def show(words: list):
    """Show words on screen, using print()"""
    for ordered in words:
        print(ordered, end="")
    print()


def main():
    words: list = sys.argv[1:]
    ordered: list = sort(words)
    for pivot_pos in range(len(words)):
        lower_pos: list = find_lower(words, pivot_pos)
        if lower_pos != pivot_pos:
            words[pivot_pos], words[lower_pos] = words[lower_pos], words[pivot_pos]
    show(ordered)



if __name__ == '__main__':
    main()

